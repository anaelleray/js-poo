/**
 * Classe représentant une tâche à faire avec une description de
 * la tâche et une propriété done indiquant si la tâche est faite ou non
 */
class Task {
    /**
     * @param {string} paramLabel Description de la tâche à faire
     */
    constructor(paramLabel) {
        this.label = paramLabel;
        this.done = false;
    }
    /**
     * Méthode qui change la propriété done de true à false ou
     * inversement
     */
    toggleDone() {
        this.done = !this.done;

        // if(this.done) {
        //     this.done = false;
        // } else {
        //     this.done = true;
        // }


    }
    /**
     * Méthode qui génère les éléments html d'une instance de task
     * @return {Element} Le html de la task
     */
    draw() {
        //On crée un élément li qui représentera le contenant de notre tâche
        let li = document.createElement('li');
        //On lui assigne comme texte la valeur actuelle de la propriété label
        li.textContent = this.label;
        //On crée un input qui sera la coche lié à la propriété done de la tâche
        let checkbox = document.createElement('input');
        checkbox.type = 'checkbox';
        //On ajoute la checkbox dans le li
        li.appendChild(checkbox);
        //Si jamais la propriété done est true, on coche la checkbox
        if (this.done) {
            checkbox.checked = true;
        }
        //On ajoute un event sur la checkbox pour faire que lorsqu'on la
        //coche ou la décoche, ça change la valeur de la propriété done
        checkbox.addEventListener('input', () => this.toggleDone());
        //on return le li pour qu'il soit utilisable à l'extérieur de la classe
        return li;
    }
}

