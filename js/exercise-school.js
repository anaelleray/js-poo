let student1 = new Student('emma', 17, 'promo12', false);

let student2 = new Student('alex', 17, 'promo12', false);

let student3 = new Student('michel', 16, 'promo12', false);

let student4 = new Student('eliott', 20, 'promo12', false);

let student5 = new Student('marc', 19, 'promo12', false);

let teacher1 = new Teacher('Mr Vernier', 'histoire',false);

let teacher2 = new Teacher('Mme Vella', 'espagnol', false);

let school = new School('Simplon')

school.students.push(student1, student2, student3, student4, student5);
school.teachers.push(teacher1, teacher2);



console.log(student1);
console.log(student2);
console.log(student3);
console.log(student4);
console.log(student5);
console.log(teacher1);
console.log(teacher2);
console.log(school);
school.teacherOut(teacher2);