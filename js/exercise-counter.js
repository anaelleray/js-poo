let instance = new Counter();
let instance2 = new Counter();

instance.increment();

console.log(instance.value);
console.log(instance2.value);

instance.decrement();
instance.decrement();
instance.decrement();

console.log(instance.value);
console.log(instance2.value);

instance.reset()

console.log(instance.value);
console.log(instance2.value);

