
/**
 * Une classe est un élément central de la POO. On peut voir ça comme
 * un moule/une usine permettant de créer des objets correspondant à ce
 * que l'on a définit dans la classe.
 * Ici, on va pouvoir utiliser cette classe pour créer des chiens,chaque
 * chien sera une "instance" de la classe Dog.
 * En terme de convention de nommage, les nom de classes sont en
 * capitalized camel case

 * (normalement une classe est seule dans son propre fichier par convention)
 */
class Dog {
    /**
     * Une classe peut contenir un constructeur. C'est une fonction 
     * particulière qui sera appelée au moment de faire une instance 
     * de la classe en question et qui, comme son nom l'indique, servira
     * à construire l'instance.
     * @param {string} paramName 
     * @param {string} paramBreed 
     * @param {string} paramPersonality 
     * @param {string} paramBirthdate 
     * @param {string} paramSize 
     */
    constructor(paramName, paramBreed, paramPersonality, paramBirthdate, paramSize) {
        /**
         * En javascript, c'est dans le constructeur qu'on définit les
         * propriétés de la classe (une propriété c'est un peu comme
         * une variable, mais liée à un objet).
         * Ici, on assigne à chaque propriété la valeur correspondante 
         * passée lors de l'appel du constructeur
         */
        this.name = paramName;
        this.breed = paramBreed;
        this.personality = paramPersonality;
        this.birthdate = paramBirthdate;
        this.size = paramSize;        
    }

    

}

/**
 * Pour créer une instance de classe, on utilise le mot clef new suivi 
 * du nom de la classe dont on souhaite créer une instance et entre
 * parenthèse les valeur à donner aux argument du constructeur de la
 * classe, si argument il y a
 */
let dog = new Dog('Fido', 'corgi', 'calm', '10/03/2016', 'medium');

presentation(dog);

let dog2 = new Dog('Rex', 'doggo', 'cool', '20/03/2015', 'big');

presentation(dog2);

/**
 * Fonction qui crée un paragraphe de présentation et l'append
 * @param {Dog} paramDog Le chien dont on veut faire le paragraphe
 */
function presentation(paramDog) {
    let p = document.createElement('p');
    p.textContent = `Henlo, my name is ${paramDog.name}, I am a ${paramDog.size} and ${paramDog.personality} ${paramDog.breed} born on ${paramDog.birthdate}. I am good dog.`;
    let target = document.querySelector('#target');
    target.appendChild(p);
}

/**
 * Ici, une fonction qui crée un objet chien. C'est une manière moins 
 * bien et un peu péter que ce qu'on fait avec une class et un
 * constructeur mais c'est pour l'idée de ce qu'il se passe lorsqu'on
 * instancie un objet
 * /!\ A ne pas utiliser en vrai /!\
 */
function createDog(paramName, paramBreed, paramPersonality, paramBirthdate, paramSize) {

    let myDog = {
        name: paramName,
        breed: paramBreed,
        personality: paramPersonality,
        birthdate: paramBirthdate,
        size: paramSize
    };
    return myDog;
}
